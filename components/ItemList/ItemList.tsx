import classnames from "classnames";
import { useContext, useEffect, useState } from "react";
import { Button, Form, Input, Segment } from "semantic-ui-react";
import db from "../../firebase/db";
import AuthContext from "../Auth/AuthContext";
import Flex from "../Flex/Flex";
import styles from "./ItemList.module.scss";

type Item = {
  name: string;
  id: string;
};

const ItemList = () => {
  const { user } = useContext(AuthContext);
  const [items, setItems] = useState<Item[]>([]);
  const [inputValue, setInputValue] = useState<string>("");

  useEffect(() => {
    return (
      db
        .collection("items")
        .where("uid", "==", user.uid)
        //.get().then(querySnapshot)
        .onSnapshot((querySnapshot) => {
          const userItems: Item[] = [];
          querySnapshot.forEach((doc) => {
            const { name } = doc.data();
            userItems.push({ id: doc.id, name });
          });
          setItems(userItems);
        })
    );
  }, [user]);

  const onChange = (event, data) => {
    setInputValue(data.value);
  };

  const onSubmit = () => {
    const name = inputValue;
    setInputValue("");
    db.collection("items")
      .add({ name, uid: user.uid })
      .then((docRef) => {
        console.log(`Document created: ${docRef.id}`);
      })
      .catch((error) => {
        console.error(`Error while creating document: `, error);
      });
  };

  const createDeleteHandler = (itemId) => {
    return () => {
      db.collection("items")
        .doc(itemId)
        .delete()
        .then(() => {
          console.log(`Document deleted: ${itemId}`);
        })
        .catch((error) => {
          console.error(`Error while deleting document: `, error);
        });
    };
  };

  return (
    <>
      <Segment>
        <Flex className={styles.table} vertical>
          <Flex className={classnames(styles.row, styles.header)}>
            <Flex className={styles.cell}>Items</Flex>
            <Flex className={styles.cell}>&nbsp;</Flex>
          </Flex>
          {items.map((item) => (
            <Flex className={styles.row} key={item.id}>
              <Flex middle className={styles.cell}>
                {item.name}
              </Flex>
              <Flex className={styles.cell}>
                <Button size="mini" onClick={createDeleteHandler(item.id)}>
                  delete
                </Button>
              </Flex>
            </Flex>
          ))}
        </Flex>
        <Flex className={styles.form}>
          <Form size="mini">
            <Form.Group inline>
              <Form.Field>
                <Input placeholder="item name" value={inputValue} onChange={onChange} />
              </Form.Field>
              <Button size="mini" color="teal" disabled={inputValue === ""} onClick={onSubmit}>
                Add Item
              </Button>
            </Form.Group>
          </Form>
        </Flex>
      </Segment>
    </>
  );
};

export default ItemList;
