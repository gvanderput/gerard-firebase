import { Container, Menu } from "semantic-ui-react";

export default function MainMenu() {
  return (
    <Menu fixed="top" inverted color="violet" style={{ border: "5px solid white" }}>
      <Container fluid>
        <Menu.Item header>Firebase Authentication</Menu.Item>
      </Container>
    </Menu>
  );
}
