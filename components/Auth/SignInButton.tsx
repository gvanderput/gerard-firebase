import firebase from "firebase/app";
import { Button, Message } from "semantic-ui-react";
import Flex from "../Flex/Flex";

const SignInButton = () => {
  const signIn = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider);
  };

  return (
    <Flex vertical>
      <Flex center middle style={{ paddingBottom: 10 }}>
        <Message info size="tiny" style={{ flexGrow: 1, textAlign: "center" }}>
          You are not signed in.
        </Message>
      </Flex>
      <Flex center middle>
        <Button size="tiny" color="blue" onClick={() => signIn()}>
          Sign in with Google
        </Button>
      </Flex>
    </Flex>
  );
};

export default SignInButton;
