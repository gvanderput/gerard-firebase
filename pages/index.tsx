import { useContext } from "react";
import AuthContext from "../components/Auth/AuthContext";
import ItemList from "../components/ItemList/ItemList";
import UserInfo from "../components/UserInfo/UserInfo";

export default function Home() {
  const { user } = useContext(AuthContext);

  return (
    <div>
      <UserInfo />
      <ItemList />
    </div>
  );
}

export async function getStaticProps() {
  return {
    props: {},
  };
}
