import "semantic-ui-css/semantic.min.css";
import { Button, Container, Icon, Segment } from "semantic-ui-react";
import AuthContextProvider from "../components/Auth/AuthContextProvider";
import MainMenu from "../components/MainMenu/MainMenu";
import "../firebase/bootstrap";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <div>
      <MainMenu />
      <Container style={{ padding: "60px 0 20px 0" }}>
        <Segment>
          <AuthContextProvider>
            <Component {...pageProps} />
          </AuthContextProvider>
        </Segment>
      </Container>
      <div style={{ textAlign: "center" }}>
        <a href="https://gerardvanderput.com" target="_blank">
          <Button icon size="mini" labelPosition="left">
            <Icon name="heart" color="red" />
            Gerard van der Put.com
          </Button>
        </a>
      </div>
    </div>
  );
}

export default MyApp;
